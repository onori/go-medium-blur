# go-medium-blur

go-medium-blur is a library for creating medium-like blur images. It’s written in Pure Go.

![img](./image.png)

### Example Server

[Go Medium Blur](https://go-medium-blur-gl4y4ylzca-an.a.run.app/)

### Repo

[https://gitlab.com/onori/go-medium-blur](https://gitlab.com/onori/go-medium-blur)

## Installation

- `git clone git@gitlab.com:onori/go-medium-blur.git`
- export env `PORT=1323` and `GO111MODULE=on`
- `go run main.go`

### Run Locally

- `docker build -t go-medium-blur:latest .`
- `docker run docker run --rm -p 1323:1323 -e PORT=1323 go-medium-blur:latest`

### Run Server

`AWS Lambda` / `Google Cloud Functions` / `Heroku`...
