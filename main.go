package main

import (
	"fmt"
	"image"
	"image/color"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"

	"github.com/disintegration/imaging"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

// Response HTTPResponse
type Response struct {
	Err    error      `json:"err"`
	Images *ImageKind `json:"images"`
}

// ImageKind Images by type
type ImageKind struct {
	Blur     Image `json:"blur"`
	Optimize Image `json:"optimize"`
	Origin   Image `json:"origin"`
}

// Image Image meta data
type Image struct {
	Path string `json:"path"`
	Size int64  `json:"size"`
}

// refPath 外部参照パス
const refPath = "uploads"

// uploadPath アップロードされたファイルの保存先
const uploadPath = "public/" + refPath

// maxFileSize アップロードされたファイルの最大値(10MB)
const maxFileSize = 10000000

// Resize対象となる画像サイズの閾値(1MB)
const resizeThreshold = 1000000

func main() {
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Static("/", "public")
	e.GET("/index", index)
	e.POST("/upload", upload)

	e.Logger.Fatal(e.Start(":" + os.Getenv("PORT")))
}

// index indexHandler
// Response Sample images.
func index(c echo.Context) error {
	r := &Response{
		Err: nil,
		Images: &ImageKind{
			Blur: Image{
				Path: "images/blur.jpg",
				Size: mustFileSize("public/images/blur.jpg"),
			},
			Optimize: Image{
				Path: "images/optimize.jpg",
				Size: mustFileSize("public/images/optimize.jpg"),
			},
			Origin: Image{
				Path: "images/origin.jpg",
				Size: mustFileSize("public/images/origin.jpg"),
			},
		},
	}
	return c.JSON(http.StatusOK, r)
}

// upload uploadHandler
func upload(c echo.Context) error {
	file, err := c.FormFile("file")

	if err != nil {
		r := &Response{
			Err:    err,
			Images: nil,
		}
		return c.JSON(http.StatusInternalServerError, r)
	}

	fileSize, err := filterFileSize(file)
	if err != nil {
		r := &Response{
			Err:    err,
			Images: nil,
		}
		return c.JSON(http.StatusBadRequest, r)
	}

	src, err := file.Open()
	if err != nil {
		r := &Response{
			Err:    err,
			Images: nil,
		}
		return c.JSON(http.StatusInternalServerError, r)
	}
	defer src.Close()

	// Destination
	originPath := fmt.Sprintf("%s/%s", uploadPath, addSuffix("origin", file.Filename))
	origin, err := os.Create(originPath)
	if err != nil {
		r := &Response{
			Err:    err,
			Images: nil,
		}
		return c.JSON(http.StatusInternalServerError, r)
	}
	defer origin.Close()

	// Copy
	if _, err = io.Copy(origin, src); err != nil {
		r := &Response{
			Err:    err,
			Images: nil,
		}
		return c.JSON(http.StatusInternalServerError, r)
	}

	// optimize
	kind, err := optimize(file.Filename, originPath, fileSize)
	if err != nil {
		r := &Response{
			Err:    err,
			Images: nil,
		}
		return c.JSON(http.StatusInternalServerError, r)
	}

	r := &Response{
		Err:    nil,
		Images: kind,
	}

	return c.JSON(http.StatusOK, r)
}

func optimize(fileName string, originPath string, size int64) (*ImageKind, error) {
	src, err := imaging.Open(originPath)
	if err != nil {
		return nil, err
	}

	k := &ImageKind{}

	originFileName := addSuffix("origin", fileName)
	k.Origin = Image{
		Path: createRefPath(originFileName),
		Size: size,
	}

	// resizeThreshold(リサイズ閾値)を確認し、リサイズ
	// resizeThreshold以下の場合は、そのままoriginを利用
	var dist = src
	if size >= resizeThreshold {
		dist = resize(src, size)
	}

	distPath := fmt.Sprintf("%s/%s", uploadPath, fileName)
	err = imaging.Save(dist, distPath)
	if err != nil {
		return nil, err
	}

	k.Optimize = Image{
		Path: createRefPath(fileName),
		Size: mustFileSize(distPath),
	}

	// blur
	blurDist := blur(dist)
	blurFileName := addSuffix("blur", fileName)
	blurPath := fmt.Sprintf("%s/%s", uploadPath, blurFileName)
	err = imaging.Save(blurDist, blurPath)
	if err != nil {
		return nil, err
	}

	k.Blur = Image{
		Path: createRefPath(blurFileName),
		Size: mustFileSize(blurPath),
	}

	return k, nil
}

// resize 画像ファイルをリサイズ 縦横比を維持
func resize(src image.Image, size int64) *image.NRGBA {
	ratio := 500 / (float64(size) / 1000)
	w := int(ratio*float64(src.Bounds().Dx()) + 0.5)
	y := int(ratio*float64(src.Bounds().Dy()) + 0.5)

	resize := imaging.Resize(src, w, y, imaging.Lanczos)
	resizeDist := imaging.New(w, y, color.NRGBA{0, 0, 0, 0})
	return imaging.Paste(resizeDist, resize, image.Pt(0, 0))
}

// blur Blur画像を作成
func blur(src image.Image) *image.NRGBA {
	blur := imaging.Blur(src, 50)
	blurDist := imaging.New(200, 200, color.NRGBA{0, 0, 0, 0})
	return imaging.Paste(blurDist, blur, image.Pt(0, 0))
}

// addSuffix suffixで指定された文字列をファイル名に追加
func addSuffix(suffix string, fileName string) string {
	ext := filepath.Ext(fileName)
	name := filepath.Base(fileName[:len(fileName)-len(ext)])
	return fmt.Sprintf("%s-%s%s", name, suffix, ext)
}

// filterFileSize アップロードファイルのサイズ上限
func filterFileSize(fileHeader *multipart.FileHeader) (int64, error) {
	byte := fileHeader.Size
	if byte >= maxFileSize {
		return 0, fmt.Errorf("File size is too large, Please specify a file smaller than %d bytes", maxFileSize)
	}
	return byte, nil
}

// mustFileSize ファイルサイズをos.Statから取得 デモなのでエラーは握りつぶす
func mustFileSize(path string) int64 {
	info, _ := os.Stat(path)
	return info.Size()
}

// createRefPath 外部参照（ブラウザ）パス
func createRefPath(fileName string) string {
	return fmt.Sprintf("%s/%s", refPath, fileName)
}
