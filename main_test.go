package main

import (
	"testing"
)

func Test_addSuffixFileName(t *testing.T) {
	type args struct {
		suffix   string
		fileName string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"Success:1",
			args{
				suffix:   "blur",
				fileName: "test.png",
			},
			"test-blur.png",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := addSuffix(tt.args.suffix, tt.args.fileName); got != tt.want {
				t.Errorf("addSuffix() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_createRefPath(t *testing.T) {
	type args struct {
		fileName string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Success:1",
			args: args{
				fileName: "exmaple.png",
			},
			want: "uploads/exmaple.png",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := createRefPath(tt.args.fileName); got != tt.want {
				t.Errorf("createRefPath() = %v, want %v", got, tt.want)
			}
		})
	}
}
